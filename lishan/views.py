from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'index.html')

def lishan(request):
    return render(request,'lishan.html')