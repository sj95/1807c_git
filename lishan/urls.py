from django.urls import path, include
from . import views
app_name='lishan'

urlpatterns = [
    path('',views.index,name='index'),
    path('lishan/',views.lishan,name='lishan')
]