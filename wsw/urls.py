from django.urls import path, include
from django.views.generic.base import TemplateView
from wsw import views

app_name = 'wsw'

urlpatterns = [
    path('',views.index,name='index'),
    path('wsw/',views.index2,name='wsw'),
]
