from django.urls import path, include
from . import views
app_name='lilei'

urlpatterns = [
    path('',views.index,name='index'),
    path('lilei/',views.lilei,name='lilei')
]