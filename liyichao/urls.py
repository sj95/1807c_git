from django.urls import path, include
from django.views.generic.base import TemplateView
from . import views
app_name='liyichao'
urlpatterns = [
    path('',views.index,name='index'),
    path('liyichao/',views.liyichao,name='liyichao')
]
