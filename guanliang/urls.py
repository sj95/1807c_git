from django.urls import path
from guanliang import views

app_name = 'guanliangliang'
urlpatterns = [
    path('guanliang/', views.index, name='guan')
]