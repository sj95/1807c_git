from django.urls import path
from fxy import views

app_name = 'fxy'
urlpatterns = [
    path('fxy/',views.fxy,name='fxy')
]