from django.urls import path, include
from . import views
app_name='shijian'

urlpatterns = [
    path('',views.index,name='index'),
    path('shijian/',views.shijian,name='shijian')
]