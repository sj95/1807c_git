from django.urls import path, include
from . import views
app_name='dusen'

urlpatterns = [
    path('',views.index,name='index'),
    path('dusen/',views.dusen,name='dusen')
]