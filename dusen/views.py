from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'index.html')

def dusen(request):
    return render(request,'dusen.html')
