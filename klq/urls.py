from klq import views
from django.urls import path, include

app_name = 'klq'

urlpatterns=[
    path('klq/',views.klq,name='klq'),
]