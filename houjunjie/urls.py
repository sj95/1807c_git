from houjunjie import views
from django.urls import path
app_name = 'houjunjie'
urlpatterns = [
    path('', views.houjunjie, name='houjunjie')
]