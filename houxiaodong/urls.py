from django.urls import path
from houxiaodong import views

app_name = 'hxd'
urlpatterns = [
    path('hxd/', views.hxd, name='hxd'),
]