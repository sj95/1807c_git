from django.urls import path, include
from . import views
app_name='slq'

urlpatterns = [
    path('',views.slq,name='slq')
]