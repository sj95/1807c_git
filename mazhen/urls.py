from django.urls import path, include
from . import views
app_name='mazhen'

urlpatterns = [
    path('',views.index,name='index'),
    path('mazhen/',views.mazhen,name='mazhen')
]