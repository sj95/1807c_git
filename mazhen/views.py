from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'index.html')

def mazhen(request):
    return render(request,'mazhen.html')
