"""git_cs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.views.generic.base import TemplateView

urlpatterns = [

    path('lishan',include('lishan.urls')),
    path('',TemplateView.as_view(template_name='index.html')),


    path('huoyanan/',include('huoyanan.urls')),

    path('klq/',include('klq.urls')),
    path('hbj/',include('hbj.urls')),
    path('dusen/',include('dusen.urls')),
    path('zhanghao/',include('zhanghao.urls')),
    path('lilei/', include('lilei.urls')),


    # path('zhanghao/',include('zhanghao.urls')),

    #path('zhanghao/',include('zhanghao.urls')),



    # path('zhanghao/',include('zhanghao.urls')),




    path('fxy/',include('fxy.urls')),

    path('hxd/', include('houxiaodong.urls')),


    path('guanliang/', include('guanliang.urls')),






    # path('hxd/', include('houxiaodong.urls')),


    #path('hxd/', include('houxiaodong.urls')),


    path('wsw/',include('wsw.urls')),



    path('houjunjie/', include('houjunjie.urls')),


    path('wangmengli/', include('wangmwngli.urls')),
    path('gonghongyi/', include('guohongyi.urls')),



    path('slq/', include('slq.urls')),




    
    path('mazhen/',include('mazhen.urls')),
    path('liyichao',include('liyichao.urls')),
    path('shijian/',include('shijian1807.urls')),



    path('chenghao/',include('chenghao.urls')),



    # path('zhangruiqiang/',include('zhangruiqiang.urls'))

    # path('zhangruiqiang/',include('zhangruiqiang.urls')),





    path("sxh/",include("sxh.urls")),




    path('zhangruiqiang/',include('zhangruiqiang.urls')),




    # path('zhangruiqiang/',include('zhangruiqiang.urls')),




    path('wjj/',include('wangjunjie.urls')),

]
